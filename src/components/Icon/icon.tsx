import React from 'react'
import classnames from 'classnames'
import { FontAwesomeIcon, FontAwesomeIconProps } from '@fortawesome/react-fontawesome'
// import CloseIcon from '../Icon/close-icon'


// interface BaseIconProps {
// 	className?: string
// 	width?: string | number
// 	height?: string | number
// }

export type IconProps = FontAwesomeIconProps
// type ButtonProps = NativeButtonProps & AnchorButtonProps

const Icon = (props: IconProps) => {
	const { className, ...restProps } = props
	const classPrefix = 'icon'
	const classes = classnames(classPrefix, className, {
	})

	return (
		<FontAwesomeIcon className={classes} {...restProps} />
	)
}

export default Icon
