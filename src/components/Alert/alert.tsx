import React, { useRef, useState } from 'react'
import classnames from 'classnames'
import Icon from '../Icon/icon'
import { faXmark } from '@fortawesome/free-solid-svg-icons'
// import CloseIcon from '../Icon/close-icon'

export type AlertType = 'success' | 'warning' | 'info' | 'danger'

interface BaseAlertProps {
	type?: AlertType
	className?: string
	message: string
	description?: string
	closable?: boolean
}

export type AlertProps = BaseAlertProps
// type ButtonProps = NativeButtonProps & AnchorButtonProps

const Alert = (props: AlertProps) => {
	const { className, message, description, type, closable, ...restProps } = props
	const alertRef = useRef(null)
	const classPrefix = 'alert'
	const classes = classnames('alert', className, {
		[`${classPrefix}-${type}`]: type
	})

	return (
		<div ref={alertRef} className={classes} {...restProps}>
			<div className='alert-header'>
				<h6>{message}</h6>
				{closable ? <Icon className='alert-close' onClick={() => {
					alertRef.current && (alertRef.current as HTMLElement).remove()
				}} icon={faXmark} /> : '' }
			</div>
			<div className="alert-description">{description}</div>
		</div>
	)
}

Alert.defaultProps = {
	type: 'info',
}

export default Alert
