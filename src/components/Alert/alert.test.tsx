import React from 'react'
import { describe, expect, test } from 'vitest'
import { render } from '@testing-library/react'
import Alert, { AlertProps } from './alert'

const defaultProps: AlertProps = {
  message: 'title',
  description: 'content'
}

const successProps: AlertProps = {
  type: 'success',
  message: 'success title',
  description: 'success content'
}

const closableProps: AlertProps = {
  type: 'danger',
  message: 'danger title',
  description: 'danger content',
  closable: true
}

describe('test Alert component', () => {
  test('should render the correct default alert', () => {
    const wrapper = render(<Alert {...defaultProps} />)
    const element =  wrapper.container.children[0]
    expect(element).toHaveClass('alert', 'alert-info')
    const messageElement = wrapper.getByText(defaultProps.message as string)
    expect(messageElement).toBeInTheDocument()
    expect(messageElement.tagName).toEqual('H6')
    const descriptionElement = wrapper.getByText(defaultProps.description as string)
    expect(descriptionElement).toBeInTheDocument()
    expect(descriptionElement).toHaveClass('alert-description')
    expect(descriptionElement.tagName).toEqual('DIV')
  })

  test('should render the correct success alert', () => {
    const wrapper = render(<Alert {...successProps} />)
    const element =  wrapper.container.children[0]
    expect(element).toHaveClass('alert', 'alert-success')
  })

  test('should render the correct closable alert', async () => {
    const wrapper = render(<Alert {...closableProps} />)
    const element =  wrapper.container.children[0]
    expect(element).toHaveClass('alert', 'alert-danger')

    const closeElement = wrapper.container.querySelector('.alert-close') as HTMLElement
    expect(closeElement).toBeInTheDocument()
    expect(closeElement.tagName).toEqual('SPAN')
  })

})
