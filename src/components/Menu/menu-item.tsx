import React, { CSSProperties, useContext, useRef, useState } from 'react'
import classnames from 'classnames'
import { MenuContext, MenuPropsItem, SelectParams } from './menu'
// import Icon from '../Icon/icon'
// import { faXmark } from '@fortawesome/free-solid-svg-icons'
// import CloseIcon from '../Icon/close-icon'


interface BaseMenuItemProps {
	style: CSSProperties
	children: React.ReactNode
	onSelect: (index: string) => void
}

export type MenuItemProps = Partial<BaseMenuItemProps> & {
	// key: React.Key
	item: MenuPropsItem
}
// type ButtonProps = NativeButtonProps & AnchorButtonProps

export type MenuItemComponent = React.FC<MenuItemProps>

const MenuItem: MenuItemComponent = (props: MenuItemProps) => {
	const { children, onSelect, item, ...restProps } = props
	const context = useContext(MenuContext)
	const classPrefix = 'menu-item'
	const classes = classnames(classPrefix, {
		[`${classPrefix}-selected`]: context.index === item.key
	})
	
	const onClick = typeof onSelect === 'function' ? (e: React.MouseEvent) => onSelect(item.key.toString()) : () => ({})
	return (
		<div className={classes} {...restProps} onClick={(e) => onClick(e)}>
			{children}
		</div>
	)
}

MenuItem.defaultProps = {
}

export default MenuItem
