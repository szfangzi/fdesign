import React, { createContext, CSSProperties, useRef, useState } from 'react'
import classnames from 'classnames'
import MenuItem, { MenuItemComponent } from './menu-item'
import SubMenu, { SubMenuComponent } from './sub-menu'
import MenuItemGroup, { MenuItemGroupComponent } from './menu-item-group'
// import { faXmark } from '@fortawesome/free-solid-svg-icons'
// import CloseIcon from '../Icon/close-icon'

type MenuMode = 'vertical' | 'horizontal'

export interface MenuPropsItem {
	key: React.Key
	label: string
	icon?: React.ReactNode
	children?: MenuPropsItem[]
	type?: string
}

export interface SelectParams {
	item: MenuPropsItem
	key: React.Key
	keyPath: React.Key[]
}

interface BaseMenuProps {
	className?: string
	defaultIndex?: string
	defaultOpenSubMenus?: string[]
	style: CSSProperties
	title: string
	mode: MenuMode
	onSelect?: (selectedIndex: string) => void
	// children: React.ReactNode
}

export type MenuProps = Partial<BaseMenuProps> & {
	items: MenuPropsItem[]	
}
// type ButtonProps = NativeButtonProps & AnchorButtonProps

export type MenuComponent = React.FC<MenuProps> & {
	Item: MenuItemComponent
	SubMenu: SubMenuComponent
	ItemGroup: MenuItemGroupComponent
}

interface IMenuContext {
	index: string
	onSelect?: (selectedIndex: string) => void
	handleOpen?: (defaultOpenSubMenus: string[]) => void
	mode?: MenuMode
	defaultOpenSubMenus: string[]
}

export const MenuContext = createContext<IMenuContext>({ index: '0', defaultOpenSubMenus: [] })

// function menuItemsToExtra(items: MenuPropsItem[]): MenuPropsItemExtra[] {
// 	const results: MenuPropsItemExtra[] = []
// 	for (let i = 0; i < items.length; i++) {
// 		const item: MenuPropsItemExtra = Object.assign({}, items[i], { expanded: false, selected: false, children: [] })
// 		if (items[i].children) {
// 			item.children = menuItemsToExtra(items[i].children as MenuPropsItem[])
// 		}
// 		results.push(item)
// 	}
// 	return results
// }

const Menu: MenuComponent = (props: MenuProps) => {
	const { className, mode, items, onSelect, defaultOpenSubMenus = [], defaultIndex, ...restProps } =
		props
	const [currentActive, setActive] = useState(defaultIndex)
	const classPrefix = 'menu'
	const classes = classnames(classPrefix, className, {
		[`${classPrefix}-${mode}`]: true
	})

	const handleClick = (index: string) => {
		setActive(index)
		if (onSelect) {
			onSelect(index)
		}
	}
	const handleOpen = (defaultOpenSubMenus: string[]) => {
		passedContext.defaultOpenSubMenus = defaultOpenSubMenus
	}
	const passedContext: IMenuContext = {
		index: currentActive ? currentActive : '0',
		onSelect: handleClick,
		mode,
		defaultOpenSubMenus,
		handleOpen
	}

	return (
		<div className={classes} {...restProps}>
			<MenuContext.Provider value={passedContext}>
				{items.map((item) => {
					if (!item.children) {
						return (
							<MenuItem key={item.key} item={item} onSelect={handleClick}>
								{item.label}
							</MenuItem>
						)
					}
					if (!item.type) {
						return <SubMenu key={item.key} items={item.children} title={item.label} lvl={0} />
					}
					return <MenuItemGroup key={item.key} items={item.children} title={item.label} lvl={1} />
				})}
			</MenuContext.Provider>
		</div>
	)
}

Menu.Item = MenuItem
Menu.SubMenu = SubMenu
Menu.ItemGroup = MenuItemGroup

Menu.defaultProps = {
	mode: 'vertical',
	defaultIndex: '0',
	defaultOpenSubMenus: [],
}

export default Menu
