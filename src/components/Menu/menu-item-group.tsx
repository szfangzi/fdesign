import React, { CSSProperties, useRef, useState } from 'react'
import classnames from 'classnames'
import { MenuPropsItem } from './menu'
import MenuItem from './menu-item'
import SubMenu from './sub-menu'
// import Icon from '../Icon/icon'
// import { faXmark } from '@fortawesome/free-solid-svg-icons'
// import CloseIcon from '../Icon/close-icon'

interface BaseMenuItemGroupProps {
	className?: string
	title: string
	// children: React.ReactNode
	items: MenuPropsItem[]
}

export type MenuItemGroupProps = Partial<BaseMenuItemGroupProps> & {
	lvl: number
}
// type ButtonProps = NativeButtonProps & AnchorButtonProps

export type MenuItemGroupComponent = React.FC<MenuItemGroupProps>

const MenuItemGroup: MenuItemGroupComponent = (props: MenuItemGroupProps) => {
	const { title, items, lvl, ...restProps } = props
	const classPrefix = 'menu-item-group'
	// const classes = classnames(classPrefix, className, {
	// })

	return (
		<div className={classPrefix} {...restProps}>
			<div className={`${classPrefix}-title`} style={{ paddingLeft: lvl * 0.75 + 'rem' }}>
				{title}
			</div>
			<div>
				{items &&
					items.map((item) => {
						if (!item.children) {
							return (
								<MenuItem key={item.key} style={{paddingLeft: lvl * 1.5 + 'rem'}} item={item}>
									{item.label}
								</MenuItem>
							)
						}
						if (!item.type) {
							return (
								<SubMenu key={item.key} items={item.children} title={item.label} lvl={lvl + 1} />
							)
						}
						return <></>
					})}
			</div>
		</div>
	)
}

MenuItemGroup.defaultProps = {}

export default MenuItemGroup
