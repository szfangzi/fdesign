import React, { CSSProperties, useContext, useEffect, useRef, useState } from 'react'
import classnames from 'classnames'
import { MenuContext, MenuPropsItem } from './menu'
import MenuItem from './menu-item'
import MenuItemGroup from './menu-item-group'
// import Icon from '../Icon/icon'
// import { faXmark } from '@fortawesome/free-solid-svg-icons'
// import CloseIcon from '../Icon/close-icon'

interface BaseSubMenuProps {
	className?: string
	parentHeight: number
	// children: React.ReactNode
}

export type SubMenuProps = Partial<BaseSubMenuProps> & {
	lvl: number
	items: MenuPropsItem[]
	title: string
}
// type ButtonProps = NativeButtonProps & AnchorButtonProps

export type SubMenuComponent = React.FC<SubMenuProps>

const SubMenu: SubMenuComponent = (props: SubMenuProps) => {
	const { items, title, lvl = 0, ...restProps } = props
	const context = useContext(MenuContext)
	const [expanded, setExpanded] = useState<boolean>(false)
	const classPrefix = 'sub-menu'

	const styles = {
		display: expanded ? 'block' : 'none'
	}

	return (
		<div className={classPrefix} {...restProps}>
			<div
				className={`${classPrefix}-title`}
				style={{ paddingLeft: lvl * 1.5 + 'rem' }}
				onClick={() => {
					setExpanded(!expanded)
				}}
			>
				{title}
			</div>
				<div className={`${classPrefix}-children`} style={styles}>
					{items &&
						items.map((item) => {
							if (!item.children) {
								return (
									<MenuItem
										key={item.key}
										style={{ paddingLeft: (lvl + 1) * 1.5 + 'rem' }}
										item={item}
									>
										{item.label}
									</MenuItem>
								)
							}
							if (!item.type) {
								return (
									<SubMenu key={item.key} items={item.children} title={item.label} lvl={lvl + 1} />
								)
							}
							return (
								<MenuItemGroup
									key={item.key}
									items={item.children}
									title={item.label}
									lvl={lvl + 1}
								/>
							)
						})}
				</div>
		</div>
	)
}

SubMenu.defaultProps = {}

export default SubMenu
