import React from 'react'
import classnames from 'classnames'

export type ButtonSize = 'lg' | 'sm'
export type ButtonType = 'primary' | 'default' | 'danger' | 'link'

interface BaseButtonProps {
	className?: string
	disabled?: boolean
	href?: string
	size?: ButtonSize
	btnType?: ButtonType
	children: React.ReactNode
}

type NativeButtonProps = BaseButtonProps & React.ButtonHTMLAttributes<HTMLElement>
type AnchorButtonProps = BaseButtonProps & React.AnchorHTMLAttributes<HTMLElement>

export type ButtonProps = Partial<NativeButtonProps & AnchorButtonProps>

const Button = (props: ButtonProps) => {
  const { className, href, disabled, size, btnType, children, ...restProps } = props
  const classPrefix = 'btn'
	const classes = classnames(classPrefix, className, {
		[`${classPrefix}-${btnType}`]: btnType,
		[`${classPrefix}-${size}`]: size,
		disabled: btnType === 'link' && disabled,
	})

	if (btnType === 'link' && href) {
		return (
			<a className={classes} href={href} {...restProps}>
				{children}
			</a>
		)
	}
	return (
		<button className={classes} disabled={disabled} {...restProps}>
			{children}
		</button>
	)
}

Button.defaultProps = {
	disabled: false,
	btnType: 'default',
}

export default Button
