import React, { useState } from 'react'
import Alert from './components/Alert/alert'
// import styles from './App.module.scss'
import Button from './components/Button/button'
import Menu from './components/Menu'

const SubMenu = Menu.SubMenu

const menuItems = [
	{
		key: '1',
    label: 'item 1',
	},
	{
		key: '2',
		label: 'sub menu 1',
		children: [
			{
				key: '3',
				label: 'group 1',
				children: [
					{
						key: '4',
						label: 'item 2',
					},
					{
						key: '5',
						label: 'item 3',
					},
				],
				type: 'group'
			},
			{
				key: '6',
				label: 'group 2',
				children: [
					{
						key: '7',
						label: 'item 4',
					},
					{
						key: '8',
						label: 'item 5',
					},
				],
				type: 'group'
			},
		]
	},
	{
		key: '9',
		label: 'sub menu 2',
		children: [
			{
				key: '10',
				label: 'item 6',
			},
			{
				key: '11',
				label: 'item 7',
			},
			{
				key: '12',
				label: 'sub menu 3',
				children: [
					{
						key: '13',
						label: 'item 8',
					},
					{
						key: '14',
						label: 'item 9',
					},
				],
			}
		],
	},
	{
		key: '15',
		label: 'group 3',
		children: [
			{
				key: '16',
				label: 'item 10',
			},
			{
				key: '17',
				label: 'item 11',
			},
		],
		type: 'group'
	},
	{
		key: '18',
		label: 'group 4',
		children: [
			{
				key: '19',
				label: 'item 12',
			},
			{
				key: '20',
				label: 'item 13',
			},
		],
		type: 'group'
	},
	{
		key: '21',
		label: 'sub menu 4',
		children: [
			{
				key: '22',
				label: 'item 14',
			},
			{
				key: '23',
				label: 'item 15',
			},
			{
				key: '24',
				label: 'item 16',
			},
			{
				key: '25',
				label: 'item 17',
			},
		],
	}
]

function App() {
	const [count, setCount] = useState(0)
	console.log(menuItems);
	
	return (
		<>
			<div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
				<Button autoFocus>autoFocus</Button>
				<Button btnType="primary">primary</Button>
				<Button btnType="danger">danger</Button>
				<Button btnType="danger" size="lg">
					lg
				</Button>
				<Button btnType="danger" size="sm">
					sm
				</Button>
				<Button href="http://baidu.com" btnType="link" target="_blank">
					link
				</Button>
				<Button onClick={() => setCount(count + 1)}>add count</Button>
				<div>{count}</div>
			</div>
			<div style={{ width: '400px', height: '300px' }}>
				<Alert
					closable
					message="haha"
					description="content content content content content content content content content content content content"
				/>
			</div>
			<h1 className='font-weight-light'>adasd</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum maiores modi quidem veniam, expedita quis laboriosam, ullam facere adipisci, iusto, voluptate sapiente corrupti asperiores rem nemo numquam fuga ab at.</p>
			<div style={{ width: '400px', height: '300px' }}>
				<Menu style={{ width: 256 }} mode="vertical" items={menuItems} />
			</div>
		</>
	)
}

export default App
